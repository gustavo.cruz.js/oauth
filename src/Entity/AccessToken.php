<?php

declare(strict_types=1);

namespace Blazon\OAuth\Entity;

use DateTimeImmutable;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Knp\DoctrineBehaviors\Contract\Entity\TimestampableInterface;
use Knp\DoctrineBehaviors\Model\Timestampable\TimestampableTrait;
use League\OAuth2\Server\Entities\ClientEntityInterface;
use League\OAuth2\Server\Entities\ScopeEntityInterface;
use League\OAuth2\Server\Entities\Traits\AccessTokenTrait;

/**
 * Access Token
 *
 * @ORM\Table(
 *     name="access_tokens",
 *     indexes={
 *         @ORM\Index(name="idx1_access_tokens", columns={"token"}),
 *     }
 * )
 *
 * @ORM\Entity(repositoryClass="OAuth\Repository\AccessTokenRepository")
 */
class AccessToken implements AccessTokenInterface, TimestampableInterface
{
    use AccessTokenTrait;
    use TimestampableTrait;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", length=11, nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="token", type="string", length=255, nullable=false)
     */
    protected $token;

    /**
     * @var DateTimeImmutable
     *
     * @ORM\Column(name="expires", type="date", nullable=false)
     */
    protected $expires;

    /**
     * @var Boolean
     *
     * @ORM\Column(name="revoked", type="boolean", nullable=false, options={"default" : 0})
     */
    protected $revoked = false;

    /**
     * @var UserInterface
     *
     * @ORM\ManyToOne(targetEntity="UserInterface", inversedBy="accessTokens")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", onDelete="cascade")
     */
    protected $user;

    /**
     * @var ClientEntityInterface
     *
     * @ORM\ManyToOne(targetEntity="Client", inversedBy="accessTokens")
     * @ORM\JoinColumn(name="client_id", referencedColumnName="id", onDelete="cascade")
     */
    protected $client;

    /**
     * @var ScopeEntityInterface[]|ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="Scope", inversedBy="accessTokens")
     * @ORM\JoinTable(name="access_token_scopes",
     *      joinColumns={@ORM\JoinColumn(name="access_token_id", referencedColumnName="id", onDelete="cascade")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="scope_id", referencedColumnName="id", onDelete="cascade")}
     * )
     */
    protected $scopes;

    /**
     * @var RefreshToken
     *
     * @ORM\OneToOne(targetEntity="RefreshToken", mappedBy="accessToken")
     */
    protected $refreshToken;

    public function __construct()
    {
        $this->scopes = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(int $id): void
    {
        $this->id = $id;
    }

    public function getToken(): string
    {
        return $this->token;
    }

    public function setToken(string $token): void
    {
        $this->token = $token;
    }

    public function getExpires(): DateTimeImmutable
    {
        return $this->expires;
    }

    public function setExpires(DateTimeImmutable $expires): void
    {
        $this->expires = $expires;
    }

    public function isRevoked(): bool
    {
        return $this->revoked;
    }

    public function setRevoked(bool $revoked): void
    {
        $this->revoked = $revoked;
    }

    public function getUser(): UserInterface
    {
        return $this->user;
    }

    public function setUser(UserInterface $user): void
    {
        $this->user = $user;
    }

    /**
     * @return ScopeEntityInterface[]|ArrayCollection
     */
    public function getScopes()
    {
        return $this->scopes;
    }

    /**
     * @param ScopeEntityInterface[] $scopes
     */
    public function setScope(array $scopes): void
    {
        $this->scopes->clear();

        foreach ($scopes as $scope) {
            $this->addScope($scope);
        }
    }

    public function addScope(ScopeEntityInterface $scope)
    {
        if ($this->scopes->contains($scope)) {
            return;
        }

        $this->scopes->add($scope);
    }

    public function getRefreshToken(): RefreshToken
    {
        return $this->refreshToken;
    }

    public function setRefreshToken(RefreshToken $refreshToken): void
    {
        $refreshToken->setAccessToken($this);
        $this->refreshToken = $refreshToken;
    }

    /*
     * mandatory methods for oauth below
     */
    public function getIdentifier()
    {
        return $this->getToken();
    }

    public function setIdentifier($identifier): void
    {
        $this->setToken($identifier);
    }

    public function getExpiryDateTime(): DateTimeImmutable
    {
        return $this->getExpires();
    }

    public function setExpiryDateTime(DateTimeImmutable $dateTime)
    {
        $this->setExpires($dateTime);
    }

    /**
     * Not used
     *
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function setUserIdentifier($identifier): void
    {
        return;
    }

    public function getUserIdentifier()
    {
        $this->getUser()->getIdentifier();
    }

    public function getClient(): ClientEntityInterface
    {
        return $this->client;
    }

    public function setClient(ClientEntityInterface $client): void
    {
        $this->client = $client;
    }
}
