<?php

declare(strict_types=1);

namespace Blazon\OAuth\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use League\OAuth2\Server\Entities\UserEntityInterface;

interface UserInterface extends UserEntityInterface
{
    public function getScopes(): ArrayCollection;
}
