<?php

declare(strict_types=1);

namespace Blazon\OAuth\Entity;

use DateTimeImmutable;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Knp\DoctrineBehaviors\Contract\Entity\TimestampableInterface;
use Knp\DoctrineBehaviors\Model\Timestampable\TimestampableTrait;
use League\OAuth2\Server\Entities\ClientEntityInterface;
use League\OAuth2\Server\Entities\ScopeEntityInterface;

/**
 * Auth Code
 *
 * @ORM\Table(
 *     name="auth_codes",
 *     indexes={
 *         @ORM\Index(name="idx1_auth_codes", columns={"token"})
 *     }
 * )
 *
 * @ORM\Entity(repositoryClass="OAuth\Repository\AuthCodeRepository")
 */
class AuthCode implements AuthCodeInterface, TimestampableInterface
{
    use TimestampableTrait;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", length=11, nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="token", type="string", length=255, nullable=false)
     */
    protected $token;

    /**
     * @var string
     *
     * @ORM\Column(name="redirect_url", type="string", length=2000, nullable=false)
     */
    protected $redirectUrl;

    /**
     * @var DateTimeImmutable
     *
     * @ORM\Column(name="expires", type="date", nullable=false)
     */
    protected $expires;

    /**
     * @var Boolean
     *
     * @ORM\Column(name="revoked", type="boolean", nullable=false, options={"default" : 0})
     */
    protected $revoked = false;

    /**
     * @var UserInterface
     *
     * @ORM\ManyToOne(targetEntity="UserInterface", inversedBy="authCode")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", onDelete="cascade")
     */
    protected $user;

    /**
     * @var Client|ClientEntityInterface
     *
     * @ORM\ManyToOne(targetEntity="Client", inversedBy="authCodes")
     * @ORM\JoinColumn(name="client_id", referencedColumnName="id", onDelete="cascade")
     */
    protected $client;

    /**
     * @var Scope[]|ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="Scope", inversedBy="authCodes")
     * @ORM\JoinTable(name="auth_code_scopes",
     *      joinColumns={@ORM\JoinColumn(name="auth_code_id", referencedColumnName="id", onDelete="cascade")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="scope_id", referencedColumnName="id", onDelete="cascade")}
     * )
     */
    protected $scopes;

    //PlaceHolder
    /** @var integer */
    protected $userIdentifier;

    public function __construct()
    {
        $this->scopes = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(int $id): void
    {
        $this->id = $id;
    }

    public function getToken(): ?string
    {
        return $this->token;
    }

    public function setToken(?string $token): void
    {
        $this->token = $token;
    }

    public function getRedirectUrl(): string
    {
        return $this->redirectUrl;
    }

    public function setRedirectUrl(string $redirectUrl): void
    {
        $this->redirectUrl = $redirectUrl;
    }

    public function getExpires(): DateTimeImmutable
    {
        return $this->expires;
    }

    public function setExpires(DateTimeImmutable $expires): void
    {
        $this->expires = $expires;
    }

    public function isRevoked(): bool
    {
        return $this->revoked;
    }

    public function setRevoked(bool $revoked): void
    {
        $this->revoked = $revoked;
    }

    public function getUser(): UserInterface
    {
        return $this->user;
    }

    public function setUser(UserInterface $user): void
    {
        $this->user = $user;
    }

    public function getClient()
    {
        return $this->client;
    }

    public function setClient(ClientEntityInterface $client)
    {
        $this->client = $client;
    }

    public function getScopes(): array
    {
        return $this->scopes->toArray();
    }

    public function setScopes(array $scopes)
    {
        $this->scopes->clear();

        foreach ($scopes as $scope) {
            $this->addScope($scope);
        }
    }

    public function addScope(ScopeEntityInterface $scope)
    {
        if ($this->scopes->contains($scope)) {
            return;
        }

        $this->scopes->add($scope);
    }

    /*
     * mandatory methods for oauth below
     */
    public function getRedirectUri(): string
    {
        return $this->getRedirectUrl();
    }

    public function setRedirectUri($uri)
    {
        $this->setRedirectUrl($uri);
    }

    public function getIdentifier(): ?string
    {
        return $this->getToken();
    }

    public function setIdentifier($identifier)
    {
        $this->setToken($identifier);
    }

    public function getExpiryDateTime(): DateTimeImmutable
    {
        return $this->getExpires();
    }

    public function setExpiryDateTime(DateTimeImmutable $dateTime)
    {
        $this->setExpires($dateTime);
    }

    public function setUserIdentifier($identifier)
    {
        $this->userIdentifier = $identifier;
    }

    public function getUserIdentifier()
    {
        if (!empty($this->userIdentifier)) {
            return $this->userIdentifier;
        }

        return $this->user->getIdentifier();
    }
}
