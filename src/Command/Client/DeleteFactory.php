<?php

declare(strict_types=1);

namespace Blazon\OAuth\Command\Client;

use Psr\Container\ContainerInterface;

class DeleteFactory
{
    public function __invoke(ContainerInterface $container): Delete
    {
        return new Delete($container);
    }
}
