<?php

declare(strict_types=1);

namespace Blazon\OAuth\Command\Client;

use Psr\Container\ContainerInterface;

class ScopesFactory
{
    public function __invoke(ContainerInterface $container): Scopes
    {
        return new Scopes($container);
    }
}
