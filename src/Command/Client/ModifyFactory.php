<?php

declare(strict_types=1);

namespace Blazon\OAuth\Command\Client;

use Psr\Container\ContainerInterface;

class ModifyFactory
{
    public function __invoke(ContainerInterface $container): Modify
    {
        return new Modify($container);
    }
}
