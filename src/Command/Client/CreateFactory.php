<?php

declare(strict_types=1);

namespace Blazon\OAuth\Command\Client;

use Psr\Container\ContainerInterface;

class CreateFactory
{
    public function __invoke(ContainerInterface $container): Create
    {
        return new Create($container);
    }
}
