<?php

declare(strict_types=1);

namespace Blazon\OAuth\Command\Scope;

use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class Delete extends AbstractScopeCommand
{
    protected function configure(): void
    {
        $this
            ->setName('oauth:scope:delete')
            ->setDescription('Delete an existing scope')
            ->addArgument(
                'name',
                InputArgument::REQUIRED,
                'Scope name'
            );
    }

    protected function execute(
        InputInterface $input,
        OutputInterface $output
    ): int {
        $name = $input->getArgument('name');
        $this->getScopeRepository()->deleteOneByName($name);
        $output->writeln('Scope: ' . $name . ' deleted.');

        return 0;
    }
}
