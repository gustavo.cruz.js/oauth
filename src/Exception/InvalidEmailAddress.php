<?php

declare(strict_types=1);

namespace Blazon\OAuth\Exception;

class InvalidEmailAddress extends \InvalidArgumentException
{
}
