<?php

declare(strict_types=1);

namespace Blazon\OAuth\Exception;

class InvalidRedirectUrl extends \InvalidArgumentException
{
}
