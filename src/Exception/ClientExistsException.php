<?php

declare(strict_types=1);

namespace Blazon\OAuth\Exception;

class ClientExistsException extends \InvalidArgumentException
{
}
