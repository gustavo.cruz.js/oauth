<?php

declare(strict_types=1);

namespace Blazon\OAuth\Exception;

class UserNotFoundException extends \InvalidArgumentException
{
}
