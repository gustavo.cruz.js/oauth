<?php

declare(strict_types=1);

namespace Blazon\OAuth\Grant;

use League\OAuth2\Server\Grant\ClientCredentialsGrant;
use Psr\Container\ContainerInterface;

class ClientCredentialsGrantFactory
{
    public function __invoke()
    {
        return new ClientCredentialsGrant();
    }
}
