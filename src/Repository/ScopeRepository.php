<?php

declare(strict_types=1);

namespace Blazon\OAuth\Repository;

use Blazon\OAuth\Entity\UserInterface;
use Doctrine\ORM\EntityRepository;
use League\OAuth2\Server\Entities\ClientEntityInterface;
use League\OAuth2\Server\Entities\ScopeEntityInterface;
use League\OAuth2\Server\Exception\OAuthServerException;
use League\OAuth2\Server\Repositories\ScopeRepositoryInterface;
use Blazon\OAuth\Entity\Client;
use Blazon\OAuth\Entity\Scope;
use Blazon\OAuth\Exception\ScopeExistsException;
use Blazon\OAuth\Exception\ScopeNotFoundException;

class ScopeRepository extends EntityRepository implements ScopeRepositoryInterface
{
    public function getScopeEntityByIdentifier($identifier)
    {
        return $this->findOneBy(['name' => $identifier]);
    }

    /**
     * @param array $scopes
     * @param string $grantType
     * @param ClientEntityInterface|Client $clientEntity
     * @param null $userIdentifier
     * @return ScopeEntityInterface[]
     * @throws OAuthServerException
     */
    public function finalizeScopes(
        array $scopes,
        $grantType,
        ClientEntityInterface $clientEntity,
        $userIdentifier = null
    ): array {
        /** @var UserInterface $user */
        $user = $this->_em->getRepository(UserInterface::class)
            ->find($userIdentifier);

        /** @var Scope $scope */
        foreach ($scopes as $scope) {
            if (
                !$clientEntity->getScopes()->contains($scope)
                || !$user->getScopes()->contains($scope)
            ) {
                throw OAuthServerException::invalidScope($scope->getIdentifier());
            }
        }

        return $scopes;
    }

    public function getAllScopeNames(): array
    {
        $scopes = $this->createQueryBuilder('a')
            ->select('a.name')
            ->getQuery()
            ->getScalarResult();

        if (empty($scopes)) {
            return [];
        }

        return array_column($scopes, 'name');
    }

    public function createScope($name)
    {
        $exists = null;

        try {
            $exists = $this->findOneByName($name);
        } catch (ScopeNotFoundException $e) {
            // This is an expected exception.  Ignore this exception.
        }

        if ($exists) {
            throw new ScopeExistsException(
                'A scope by the name of "' . $name . '"" already exists'
            );
        }

        $scope = new Scope();
        $scope->setName($name);

        $this->_em->persist($scope);
        $this->_em->flush($scope);

        return $scope;
    }

    public function deleteOneByName($name)
    {
        $scope = $this->findOneByName($name);
        $this->_em->remove($scope);
        $this->_em->flush();
    }

    public function findOneByName($name)
    {
        $scope = $this->findOneBy(['name' => $name]);

        if (!$scope) {
            throw new ScopeNotFoundException(
                'A scope by the name of ' . $scope . ' was not found'
            );
        }

        return $scope;
    }
}
