<?php

declare(strict_types=1);

namespace Blazon\OAuth\Repository;

use Blazon\OAuth\Entity\UserInterface;
use Doctrine\ORM\EntityRepository;
use League\OAuth2\Server\Entities\AccessTokenEntityInterface;
use League\OAuth2\Server\Entities\ClientEntityInterface;
use League\OAuth2\Server\Exception\UniqueTokenIdentifierConstraintViolationException;
use League\OAuth2\Server\Repositories\AccessTokenRepositoryInterface;
use Blazon\OAuth\Entity\AccessToken;
use Blazon\OAuth\Exception\AccessTokenNotFoundException;

/** @SuppressWarnings(PHPMD.StaticAccess) */
class AccessTokenRepository extends EntityRepository implements AccessTokenRepositoryInterface
{
    public function getNewToken(ClientEntityInterface $clientEntity, array $scopes, $userIdentifier = null)
    {
        $accessToken = new AccessToken();
        $accessToken->setClient($clientEntity);
        $accessToken->setScope($scopes);

        $userRepository = $this->_em->getRepository(UserInterface::class);

        /** @var UserInterface|null $user */
        $user = $userRepository->find($userIdentifier);

        if (!$user) {
            throw new \RuntimeException(
                'Unable to locate user'
            );
        }

        $accessToken->setUser($user);

        return $accessToken;
    }

    /**
     * @param AccessTokenEntityInterface|AccessToken $accessTokenEntity
     *
     * @throws UniqueTokenIdentifierConstraintViolationException
     */
    public function persistNewAccessToken(AccessTokenEntityInterface $accessTokenEntity)
    {
        try {
            $existingToken = $this->findAccessTokenByToken($accessTokenEntity->getIdentifier());
        } catch (AccessTokenNotFoundException $e) {
            // This is an expected exception.  Ignore this exception.
        }

        if (!empty($existingToken)) {
            throw UniqueTokenIdentifierConstraintViolationException::create();
        }

        if (!$accessTokenEntity->getId()) {
            $this->_em->persist();
        }

        $this->_em->flush();
    }

    /**
     * @param string $token
     * @return AccessToken|null
     */
    public function findAccessTokenByToken($token)
    {
        /** @var AccessToken|null $accessToken */
        $accessToken = $this->findOneBy(['token' => $token]);

        if (!$accessToken) {
            throw new AccessTokenNotFoundException(
                'An Access Token by the token id of ' . $token . ' was not found'
            );
        }

        return $accessToken;
    }

    public function revokeAccessToken($tokenId)
    {
        $accessToken = $this->findAccessTokenByToken($tokenId);
        $accessToken->setRevoked(true);
    }

    public function isAccessTokenRevoked($tokenId)
    {
        $accessToken = $this->findAccessTokenByToken($tokenId);
        return $accessToken->isRevoked();
    }
}
