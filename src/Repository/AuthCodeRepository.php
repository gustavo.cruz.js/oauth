<?php

declare(strict_types=1);

namespace Blazon\OAuth\Repository;

use Blazon\OAuth\Entity\AuthCodeInterface;
use Blazon\OAuth\Entity\UserInterface;
use Blazon\OAuth\Exception\InvalidAuthCodeEntityException;
use Doctrine\ORM\EntityRepository;
use League\OAuth2\Server\Entities\AuthCodeEntityInterface;
use League\OAuth2\Server\Exception\UniqueTokenIdentifierConstraintViolationException;
use Blazon\OAuth\Entity\AuthCode;
use Blazon\OAuth\Exception\AuthCodeNotFoundException;
use League\OAuth2\Server\Repositories\AuthCodeRepositoryInterface;

class AuthCodeRepository extends EntityRepository implements AuthCodeRepositoryInterface
{
    public function getNewAuthCode(): AuthCode
    {
        return new AuthCode();
    }

    public function persistNewAuthCode(AuthCodeEntityInterface $authCodeEntity)
    {
        $exists = null;

        if ($authCodeEntity instanceof AuthCodeInterface) {
            throw new InvalidAuthCodeEntityException(
                "This repository requires an entity that implements: " . AuthCodeInterface::class
            );
        }

        try {
            $exists = $this->findOneByToken($authCodeEntity->getToken());
        } catch (AuthCodeNotFoundException $e) {
            // This is an expected exception.  Ignore this exception.
        }


        if ($exists) {
            throw UniqueTokenIdentifierConstraintViolationException::create();
        }

        /** @var UserInterface $user */
        $user = $this->_em->getRepository(UserInterface::class)
            ->find($authCodeEntity->getUserIdentifier());

        $authCodeEntity->setUser($user);

        $this->_em->persist($authCodeEntity);
        $this->_em->flush();
    }

    public function revokeAuthCode($codeId)
    {
        $authCode = $this->findOneByToken($codeId);
        $authCode->setRevoked(true);
        $this->_em->flush($authCode);
    }

    public function isAuthCodeRevoked($codeId)
    {
        $authCode = $this->findOneByToken($codeId);
        return $authCode->isRevoked();
    }

    public function findOneByToken($token): AuthCodeInterface
    {
        /** @var AuthCodeInterface $authCode */
        $authCode = $this->findOneBy(['token' => $token]);

        if (!$authCode) {
            throw new AuthCodeNotFoundException(
                'A Auth Code by the token id of ' . $token . ' was not found'
            );
        }

        return $authCode;
    }
}
